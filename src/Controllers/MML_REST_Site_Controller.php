<?php

namespace Molamil\Controllers {
        use \WP_REST_Controller;

        class MML_REST_Site_Controller extends WP_REST_Controller
        {
                protected $version;

                public function __construct( )
                {
                        $this->version   = '1';
                        $this->namespace = 'mml/' . 'v' . $this->version;
                        $this->rest_base = __('site', 'mml-wp-api' );

                        $this->repo = new \Molamil\Repositories\SiteRepository( );
                }

                public function register_routes( )
                {
                        register_rest_route(
                                $this->namespace, $this->rest_base,
                                [
                                    [
                                        'methods'             => \WP_REST_Server::READABLE,
                                        'callback'            => [ $this, 'get_site' ],
                                        'permission_callback' => [ $this, 'get_item_permissions_check' ],
                                    ]
                                ]
                        );
                }

                public function get_site( $request )
                {
                        // could put menu generation and site generation into their own classes...
                        $menus = $this->get_wp_menus_keyed_by_slug( );

                        $navigation = array_map( function( $menu )
                        {
                                $items = wp_get_nav_menu_items( $menu );
                                return $this->buildTree( $items, 0 );
                        }, $menus );

                        // dont know how to manage versions...yet.
                        $data = [
                                'navigation' => $navigation,
                                'site' => [
                                        'acf' => get_fields( 'site-general-settings' )
                                ],
                                'meta' => [
                                        'location' => parse_url( get_home_url( ) ),
                                        'root'     => '/wp-json/mml/v1/'
                                ]
                        ];

                        $response = rest_ensure_response( $data );
                        return new \WP_REST_Response( $response, 200 );

                }

                public function get_item_permissions_check( $request )
                {
                        return true;
                }

                protected function get_wp_menus_keyed_by_slug( )
                {
                        $menus = array_reduce( wp_get_nav_menus( ), function( $acc, $menu ) {
                                $acc[$menu->slug] = $menu; return $acc;
                        } , []);

                        return $menus;
                }

                private function buildTree( array &$elements, $parentId = 0 )
                {
                        $branch = [];
                        foreach ( $elements as &$element )
                        {
                                if ( $element->menu_item_parent == $parentId )
                                {
                                        $children = $this->buildTree( $elements, $element->ID );
                                        if ( $children )
                                                $element->children = $children;

                                        $branch[] = $this->prepare_menu_item( $element );
                                        unset( $element );
                                }
                        }
                        return $branch;
                }

                private function prepare_menu_item( $item )
                {
                        $object      = get_post( $item->object_id );
                        $type_object = get_post_type_object( $object->post_type );

                        switch ( $item->type . '_' . $item->object ) 
                        {
                                case 'post_type_page':
                                        $type = $type_object->name;
                                        $base = $type_object->rest_base;
                                        $path = wp_make_link_relative( get_permalink( $object->ID ) );
                                        $path = $base . rtrim( $path, '/' );

                                        if( $path == $base ):
                                                $path = '/';
                                        endif;
                                        break;
                                case 'custom_custom':
                                        $type = 'link';
                                        $path = $item->url;
                                        break;
                                default:
                                        $type = 'default';
                                        $path = 'default';
                                        break;
                        }
                        
                        $object      = get_post( $item->object_id );
                        $type_object = get_post_type_object( $object->post_type );
                        
                        $label = $object->post_title;
                        $slug  = $object->post_name;

                        $template = rtrim( get_page_template_slug( $object->ID ), '.php' );
                        $acf      = get_fields( $item );
                        $children = $item->children ?: [];

                        return [
                                'type'     => $type,
                                'label'    => $label,
                                'slug'     => $slug,
                                'path'     => $path,
                                'template' => $template,
                                'acf'      => $acf,
                                'children' => $children
                        ];
                }
        }
}
