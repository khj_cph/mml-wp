<?php

namespace Molamil\Repositories {
        use \Molamil\Interfaces\IPagesRepository;

        class PagesRepository implements IPagesRepository
        {
                public function __construct( )
                {
                        $this->default_query_args = [
                                'post_type'        => 'page',
                                'post_status'      => 'publish',
                                'suppress_filters' => false,
                                'numberposts'      => 1                               
                        ];
                }

                public function get_by_id( int $id )
                {
                        
                }

                public function get_by_slug( string $slug )
                {
                        $args = array_merge( $this->default_query_args, ['pagename' => $slug] );
                        $page = $this->get_page( $args );

                        return $page;
                }

                // protected ??
                private function get_page( $args )
                {
                        if( $page = get_posts( $args ) )
                        {
                                return $page[0];
                        }
                        else
                        {
                        }
                }
        }
}